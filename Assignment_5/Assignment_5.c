#include<stdio.h>

//declaring Functions
int noOfAttendees(tickp);
int income(int tickp);
int expenditure(int tickp);
int profit(int tickp);
void displayNComp();


int noOfAttendees(tickp){
	int avgattendance =120;
	return avgattendance - ((tickp-15)/5*20);
}

int income(int tickp){
	return noOfAttendees(tickp)*tickp;
}

int expenditure(int tickp){
	return  500+ 3*noOfAttendees(tickp);	
}

int profit(int tickp){
	return  income(tickp)-expenditure(tickp);
	
}
void displayNComp(){
	
	int x,maxtickp,oldprofit=0;
	
printf("Ticket Price(Rs.) \tProfit(Rs.)\n\n");

	//displaying all the ticket prices and the profits earn by each ticket price
	for ( x=5; x<=40; x +=5){
		
		printf("  %d \t\t\t  %d \n",x,profit(x));
				
		int newprofit= profit(x);
		
		if (newprofit>oldprofit){
			oldprofit= newprofit; //finding the highest profit
			maxtickp=x;//highest profitable ticket price
		}
	}
	printf(" \n.............................................................\n\n# The most profitable Ticket Price : Rs.%d \n# Profit Earns from that paticular Ticket Price: Rs.%d",maxtickp,oldprofit);
}

int main(){
	displayNComp();
	return 0;
}
