#include <stdio.h>

int main() {
    char ltr;
	printf("..Cheking whether entered letter is a vowel or consonant.. \n");
    printf("Enter a letter: ");
    scanf("%c", &ltr);

    if (!isalpha(ltr)) {
      printf("Entered Character is not a letter");
    }
    
    else {
      
      switch (ltr){
    case 'a' :
    case 'e' :
    case 'i' :
    case 'o' :
    case 'u' :
    case 'A' :
    case 'E' :
    case 'I' :
    case 'O' :
    case 'U' :
    printf("Entered letter is a vowel.\n");
        break;
    default :
    printf("Entered letter is a consonant.\n");
    }
    }

    return 0;
}
