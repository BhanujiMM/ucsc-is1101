#include <stdio.h>
#define PI 3.14

int main() {
	
	float r,area ;
	printf("..Calculating the area of a disk.. \n");
   	printf("Enter the radius: ");
	scanf("%f",&r);
	area= PI*r*r;
	printf("Area of the disk = %f\n",area);
	
	return 0;
}
