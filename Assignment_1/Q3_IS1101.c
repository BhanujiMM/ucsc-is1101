#include <stdio.h>

int main() {
	
	int x,y,z;
	printf("..Swapping Two Integers.. \n");
	printf("Enter First Number : ");
	scanf("%d",&x);
	printf("Enter Second Number : ");
	scanf("%d",&y);	
	z=x;
	x=y;
	y=z;
	printf("After swapping first number is now : %d \n",x);
	printf("After swapping second number is now: %d \n",y);
	
	return 0;
}
