#include <stdio.h>

int pattern(int);
int printline(int);
  
int printline(int n){
	while (n>0){
		printf("%d " ,n);
		n=n-1;}
		printf("\n");
}

int pattern(int n) {
   if(n>0) {
   pattern(n-1);} 
   printline(n);
 }

int main() {
	int nvalue,i;
	printf("Enter the number of lines(n) for the number triangle: ");
	scanf("%d",&nvalue);
	pattern(nvalue);
	
	return 0;
}
