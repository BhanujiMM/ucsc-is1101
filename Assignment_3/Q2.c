#include <stdio.h>

int main() {
    int numb, a,rem, prime = 0;
    printf("Enter a positive integer here: ");
    scanf("%d", &numb);

    if (numb==1){
    	printf("1 is not a prime number or a composite number ");
	}
	
	else {
	for (a = 2; a <= numb / 2; ++a) {
		
		rem=numb % a;
	
        if (rem == 0) {
            prime = 1;
            break;
        }
    }
	if (prime == 0){
	
	    printf("%d is a prime number.", numb);
		}
    else{
		
        printf("Entered number is not a prime number.", numb);
    }
	}
    return 0;
}
