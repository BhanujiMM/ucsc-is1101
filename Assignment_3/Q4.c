#include <stdio.h>

int main() {
    int numb, a;
    printf("Enter a number: ");
    scanf("%d", &numb);
    
    
    printf("Factors of entered number (%d) are: ", numb);
    for (a = 1; a <= numb; ++a) {
        if (numb % a == 0) {
            
			printf("%d ", a);
        }
    }
	
    return 0;
}
