#include <stdio.h>

int main() {
    int numb, rem, x = 0;
    printf("Enter a number here to be reversed : ");
    scanf("%d", &numb);
    
    while (numb != 0) {
        rem = numb % 10;
        x = x * 10 + rem;
        numb /= 10;
    }
    printf("Reversed number is = %d", x);
    return 0;
}
