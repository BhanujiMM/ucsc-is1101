#include <stdio.h>

int main()
{
    int n,a,x;
    
    
    printf("Enter the \"n\" value: ");
    scanf("%d",&n);
    printf("..Multiplication tables from 1 to n.. \n");
    printf("************************************ \n");
    
    for(a=1; a <= n; a++)
	{
    	printf("..Multiplication table of number %d.. \n",a);
    	
		for(x=1; x < 11 ;x++)
		{
    		printf("%d * %d = %d \n",a,x,a*x);
		}
		
	}
    return 0;
}

